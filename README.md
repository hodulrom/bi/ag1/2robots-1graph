# Domácí úkol 2

## Algoritmus

```
 1. Algoritmus průchod_roboty(políčko r1, políčko r2) {
 2.     mapa1 := prázdná mapa   // mapa zpětné cesty r1
 3.     mapa2 := prázdná mapa   // mapa zpětné cesty r2
 4.     mapa1[r1] := null       // null znamená prázdnou hodnotu a indikuje "ze startovního políčka se již není kam vrátit"
 5.     mapa2[r2] := null
 6.     while (r1 != null && r2 != null):
 7.         if (r1 != null):
 8.             r1 := krok(r1, mapa1)
 9.         if (r2 != null):
10.             r2 := krok(r2, mapa2)
11. }
12. 
13. // Funkce spočítá a vrátí novou pozici robota nebo null, pokud robot již prošel celý graf, vrátil se na začátek a už nemá kam jít.
14. Funkce krok(políčko r, mapa m) {
15.     označ r žlutě           // označíme uzel jako otevřený
16.     foreach (políčko t sousedící s r):
17.         if (t je označeno): continue
18.         m[t] = r            // zapíšeme si cestu o 1 krok zpět (předchozí políčko)
19.         return t            // vrátíme t jako aktuální pozici robota
20.     // pokud jsou všechny sousedící uzly aktuální pozice robota již označeny
21.     označ r zeleně          // uzavřeme aktuální uzel robota
22.     return m[r]             // vrátíme se o 1 políčko zpět
23. }
```

* neoznačený = neotevřený uzel
* žlutá = otevřený uzel
* zelená = uzavřený uzel

## Důkaz korektnosti a konečnosti

* Algoritmus projde řádky 18-19 pro jedno políčko nejvýše jednou, protože v rámci nich není políčko `t` označeno. 
* Funkce `krok` vrátí neoznačené políčko `t`, které nahradí aktuální pozici robota 1, resp. robota 2, na řádku 8, resp. na řádku 10, a v dalším běhu se určitě označí žlutě.
* Postupným označováním už nezbude neoznačený soused a políčka se začnou označovat zeleně (uzavírat) a roboti vracet zpět, dokud nenarazí na zarážku `null`. Z toho plyne konečnost algoritmu.
* Předpokládejme pro spor, že `průchod_roboty` došel do konce a existuje neoznačené políčko `v` do kterého vede z `s` cesta `P`.
* Označme `w` první neoznačené políčko na cestě P
* Předchází mu již označené políčko `w'`
* Políčko `w'` se označilo při zavolání funkce `krok` pro políčko `w'`.
* V rámci tohoto volání se určitě na řádku 19 vrátilo z funkce políčko `w`
* `w` není `null`, tudíž se funkce `krok` zavolá i pro `w`
* Políčko bylo určitě označeno (řádek 15), což je ve sporu z předpokladem, že `w` zůstalo neoznačeno.

## Odvození složitosti

### Čas

* Algoritmus projde řádky 18-19 pro jedno políčko nejvýše jednou, protože v rámci nich není políčko t označeno.
* Funkce `krok` se volá nejvýše 2n-krát, protože buďto označí políčko žlutě a postoupí do neoznačeného políčka, nebo označí políčko zeleně a vrací se do předchozího políčka. To dělá tak dlouho, dokud neoznačí startovní políčko zeleně a už se nemá kam vrátit.

Časová složitost je lineární s počtem uzlů: `O(n)`

### Paměť

* Algoritmus si pamatuje 2 mapy s nejvýše `n` páry klíč-hodnota (uzel-uzel nebo uzel-null pro startovní políčko).
* Dále si pamatuje reprezentaci grafu jako pole sousedů - nejvýše `n^2`
* Také si pamatuje označení uzlů, nejvýše `n`.

Paměťová složitost bude kvadratická s počtem uzlů: `O(n^2)`
